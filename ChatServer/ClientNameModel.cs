namespace com.bitvillain.chat.server
{
	public class ClientNameModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}