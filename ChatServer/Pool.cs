﻿using System;
using System.Collections.Concurrent;

namespace com.bitvillain.chat.server
{
	public class Pool<T>
	{
		private readonly Func<T> factoryMethod;
		private int count;
		private int maxCount;
		private ConcurrentBag<T> pool;

		public Pool(Func<T> factoryMethod, int maxCount = 10)
		{
			this.factoryMethod = factoryMethod;
			this.maxCount = maxCount;

			pool = new ConcurrentBag<T>();
			count = 0;
		}

		public T Get()
		{
			T item = default(T);
			if (!pool.TryTake(out item) && count < maxCount){
				item = factoryMethod();
				count++;
			}
			return item;
		}

		public void Put(T item)
		{
			pool.Add(item);
		}
	}
}