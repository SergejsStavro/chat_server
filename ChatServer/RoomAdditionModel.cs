namespace com.bitvillain.chat.server
{
	public class RoomAdditionModel
	{
		public int RoomID { get; set; }
		public string Token { get; set; }
	}
}