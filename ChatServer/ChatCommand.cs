﻿using System;
using System.Net.Sockets;

namespace com.bitvillain.chat.server
{
	public class ChatCommand
	{
		public string Token { get; set; }
		public ChatCommandType Type { get; set; }
		public string JSON { get; set; }
		public DateTime Timestamp { get; set; }
	}
}