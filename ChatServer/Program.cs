﻿using System;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace com.bitvillain.chat.server
{
	class Program
	{

		static void Main(string[] args)
		{
			var chatServer = new ChatServer();
			var chatLogger = new ChatServerLogger();
			chatServer.ServerStarted += chatLogger.HandleServerStarted;
			chatServer.ServerStopped += chatLogger.HandleServerStopped;
			chatServer.ServerTicked += chatLogger.HandleServerTicked;
			chatServer.ClientConnected += chatLogger.HandleClientConnected;
			chatServer.ClientDisconnected += chatLogger.HandleClientDisconnected;
			chatServer.ClientMessageSent += chatLogger.HandleClientMessageSent;
			chatServer.ClientNameChanged += chatLogger.HandleClientNameChanged;
			chatServer.RoomCreated += chatLogger.HandleRoomCreated;
			chatServer.RoomClientAdded += chatLogger.HandleRoomClientAdded;
			chatServer.RoomClientRemoved += chatLogger.HandleRoomClientRemoved;
			chatServer.RoomClientListSent += chatLogger.HandleRoomClientListSent;

			chatServer.Start();

			Console.WriteLine("Press any key to stop server.\n");
			Console.ReadLine();
			chatServer.Stop();

			Console.WriteLine("Press any key to quit.\n");
			Console.ReadLine();
		}
	}

	public class ChatServerLogger
	{
		internal void HandleClientConnected(string token)
		{
			Console.WriteLine("Client {0} connected", token);
		}

		internal void HandleClientDisconnected(string token)
		{
			Console.WriteLine("Client {0} disconnected", token);
		}

		internal void HandleClientMessageSent(string token, string name, int roomID, string message)
		{
			Console.WriteLine("Client {1}({0}) in Room {2} Wrote: {3}", token, name, roomID, message);
		}

		internal void HandleClientNameChanged(string token, string oldName, string newName)
		{
			Console.WriteLine("Client {0} Changed name from {1} to {2}", token, oldName, newName);
		}

		internal void HandleRoomCreated(string token, int roomID)
		{
			Console.WriteLine("Created {0} by {1}...", roomID, token);
		}

		internal void HandleRoomClientAdded(string token, int roomID, string addedToken)
		{
			Console.WriteLine("Added {0} to {1} by {2}...", addedToken, roomID, token);
		}

		internal void HandleRoomClientRemoved(string token, int roomID, string removedToken)
		{
			Console.WriteLine("Removed {0} from {1} by {2}...", removedToken, roomID, token);
		}

		internal void HandleRoomClientListSent(string token, int roomID)
		{
			Console.WriteLine("Client list for {0} sent to {1}...", roomID, token);
		}

		internal void HandleServerStarted(IPAddress ipAddress, int port, string hostName, IPHostEntry ipHostEntry)
		{
			string addresses = string.Empty;
			foreach (var item in ipHostEntry.AddressList) {
				addresses += string.Format(" - {0}\n", item.ToString());
			}

			Console.WriteLine("Server started @ {0}:{1}\n{2}\n{3}", 
				ipAddress.ToString(), port.ToString(), hostName.ToString(), addresses);
		}

		internal void HandleServerStopped()
		{
			Console.WriteLine("Server stopped\n");
		}

		internal void HandleServerTicked()
		{
			Console.WriteLine("Server ticked");
		}
	}
}


//Socket listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

//IPEndPoint endPoint = new IPEndPoint(address, port);

//listenerSocket.Bind(endPoint);
//listenerSocket.Listen(5);

//Console.WriteLine("Starting");

//Socket clientSocket = listenerSocket.Accept();

//Console.WriteLine("Client connected " + clientSocket.ToString() + " - IP End Point " + clientSocket.RemoteEndPoint.ToString());

//clientSocket.Send(Encoding.ASCII.GetBytes("hello"));

//byte[] buffer = new byte[128];

//int receiveBufferSize = 0;

//bool exitCommandReceived = false;

//while (!exitCommandReceived)
//{
//	receiveBufferSize = clientSocket.Receive(buffer);

//	Console.WriteLine("numberOfReceivedBytes: " + receiveBufferSize);
//	string receivedString = Encoding.ASCII.GetString(buffer, 0, receiveBufferSize);
//	Console.WriteLine("data: " + receivedString);

//	clientSocket.Send(buffer);

//	Array.Clear(buffer, 0, buffer.Length);
//	receiveBufferSize = 0;

//	exitCommandReceived = receivedString == "kill";
//}

//THIS WORKS
//TcpListener serverSocket = new TcpListener(address, port);
//TcpClient clientSocket = default(TcpClient);
//int counter = 0;

//serverSocket.Start();
//Console.WriteLine("Chat Server Started ....");
//counter = 0;
//while ((true))
//{
//	counter += 1;
//	clientSocket = serverSocket.AcceptTcpClient();

//	byte[] bytesFrom = new byte[10025];
//	string dataFromClient = null;

//	NetworkStream networkStream = clientSocket.GetStream();
//	networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
//	dataFromClient = Encoding.ASCII.GetString(bytesFrom);
//	dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));

//	clientsList.Add(dataFromClient, clientSocket);

//	//broadcast(dataFromClient + " Joined ", dataFromClient, false);

//	Console.WriteLine(dataFromClient + " Joined chat room ");
//	//handleClinet client = new handleClinet();
//	//client.startClient(clientSocket, dataFromClient, clientsList);
//}

//clientSocket.Close();
//serverSocket.Stop();
//Console.WriteLine("exit");
//Console.ReadLine();

////////////////////////////////////////////////////////

//Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
//IPEndPoint localEndPoint = new IPEndPoint(address, port);

//socket.Bind(localEndPoint);
//socket.Listen(10);

//Socket client = socket.Accept();

//IPAddress[] localIPAddresses = Dns.GetHostAddresses(Dns.GetHostName());

//foreach (IPAddress localIPAddress in localIPAddresses)
//{
//	if (localIPAddress.AddressFamily == AddressFamily.InterNetwork)
//	{
//		Console.WriteLine(localIPAddress.ToString());
//	}
//}