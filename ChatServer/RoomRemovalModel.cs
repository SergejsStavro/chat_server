namespace com.bitvillain.chat.server
{
	public class RoomRemovalModel
	{
		public int RoomID { get; set; }
		public string Token { get; set; }
	}
}