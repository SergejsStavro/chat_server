﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace com.bitvillain.chat.server
{
	public class ChatClient
	{
		public IPEndPoint RemoteEndPoint { get; set; }
		public TcpClient TcpClient { get; set; }
		public string Token { get; set; } // TODO expiration and removal
		public string Name { get; set; }
		public int CurrentRoomID { get; set; }
		public bool Connected { get { return TcpClient != null && TcpClient.Connected; } }

		public CancellationTokenSource ReadCancellationTokenSource =
			new CancellationTokenSource();

		public CancellationTokenSource WriteCancellationTokenSource =
			new CancellationTokenSource();

		public ChatClient()
		{
			Token = UniqueToken();
			Name = RandomName();
			CurrentRoomID = 0;
		}

		private string UniqueToken()
		{
			// byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
			// byte[] key = Guid.NewGuid().ToByteArray();

			// //TODO encrypt later on
			// return Convert.ToBase64String(time.Concat(key).ToArray());

			//TODO encrypt later on
			return Guid.NewGuid().ToString();
		}

		private string RandomName()
		{
			var characters = "0123456789QWERTYUIOPASDFGHJKLZXCVBNM";
			var name = new char[8];
			var generator = new Random();

			for (int i = 0; i < name.Length; i++)
				name[i] = characters[generator.Next(characters.Length)];

			return new string(name);
		}

		internal void Reset()
		{
			ReadCancellationTokenSource = new CancellationTokenSource();
			WriteCancellationTokenSource = new CancellationTokenSource();
		}

		internal bool Disconnect()
		{
			bool disconnected = false;
			ReadCancellationTokenSource.Cancel();
			WriteCancellationTokenSource.Cancel();
			if (TcpClient != null)
			{
				TcpClient.Close();
				TcpClient = null;

				disconnected = true;
			}
			return disconnected;
		}
	}
}