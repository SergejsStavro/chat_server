namespace com.bitvillain.chat.server
{
	public class ClientLoginModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}