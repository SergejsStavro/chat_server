﻿using System.Collections.Concurrent;

namespace com.bitvillain.chat.server
{
	public class ChatRoom
	{
		public int ID { get; set; }
		public ConcurrentDictionary<string, byte> Tokens =
			new ConcurrentDictionary<string, byte>();
		public ConcurrentQueue<ClientRoomMessageModel> MessageQueue =
			new ConcurrentQueue<ClientRoomMessageModel>();
	}
}