using System.Collections.Generic;

namespace com.bitvillain.chat.server
{
	public class ClientRoomListModel
	{
		public List<ClientRoomModel> Rooms { get; set; }
	}
}