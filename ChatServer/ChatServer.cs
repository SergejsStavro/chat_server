﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace com.bitvillain.chat.server
{
	public class ChatServer
	{
		bool isRunning = false;
		int messageQueueCapacity = 10;
		int messageHistoryCapacity = 10;
		int commandPoolCapacity = 10;
		int roomPoolCapacity = 10;

		int listenerPort = 8000;
		IPAddress listenerAddress = IPAddress.Any;

		string hostName;
		IPHostEntry ipHostEntry;
		IPAddress hostIPAddress;

		TcpListener tcpListener;

		ConcurrentDictionary<string, ChatClient> tokenToClient =
			new ConcurrentDictionary<string, ChatClient>();

		#region rooms
		int nextRoomID = 0;
		int globalRoomID = 0;
		ConcurrentDictionary<int, ChatRoom> roomIdToRoom =
			new ConcurrentDictionary<int, ChatRoom>();

		Pool<ChatRoom> roomPool;
		#endregion

		ConcurrentQueue<ClientRoomMessageModel> messageHistory = new ConcurrentQueue<ClientRoomMessageModel>();

		Pool<ChatCommand> commandPool;
		ConcurrentQueue<ChatCommand> commandQueue = new ConcurrentQueue<ChatCommand>();

		Dictionary<ChatCommandType, Action<ChatCommand>> commandTypeToHandler;

		public event Action<IPAddress, int, string, IPHostEntry> ServerStarted = (listenerIP, port, hostname, ipHostEntry) => { };
		public event Action ServerStopped = () => { };
		public event Action ServerTicked = () => { };

		public event Action<string> ClientConnected = (token) => { };
		public event Action<string> ClientDisconnected = (token) => { };

		public event Action<string, string, int, string> ClientMessageSent = (token, name, roomID, message) => { };
		public event Action<string, string, string> ClientNameChanged = (token, oldName, newName) => { };
		public event Action<string, int> RoomCreated = (token, roomID) => { };
		public event Action<string, int, string> RoomClientAdded = (token, roomID, tokenAdded) => { };
		public event Action<string, int, string> RoomClientRemoved = (token, roomID, tokenRemoved) => { };
		public event Action<string, int> RoomClientListSent = (token, roomID) => { };

		public ChatServer()
		{
			commandPool = new Pool<ChatCommand>(
				() => { return new ChatCommand(); },
				commandPoolCapacity
			);

			roomPool = new Pool<ChatRoom>(
				() => { return new ChatRoom() { ID = nextRoomID++ }; },
				roomPoolCapacity
			);

			commandTypeToHandler = new Dictionary<ChatCommandType, Action<ChatCommand>>()
			{

				{ ChatCommandType.ClientLogin, HandleClientLogin },

				{ ChatCommandType.ClientLogout, HandleClientLogout },

				{ ChatCommandType.ClientWrite, HandleClientWrite },

				{ ChatCommandType.ClientSetName, HandleClientSetName },

				{ ChatCommandType.RoomNew, HandleRoomNew },

				{ ChatCommandType.RoomClientAdd, HandleRoomClientAdd },

				{ ChatCommandType.RoomClientRemove, HandleRoomClientRemove },

				{ ChatCommandType.RoomClientList, HandleRoomClientList },

			};
  
	  		hostName = Dns.GetHostName();
			ipHostEntry = Dns.GetHostEntry(hostName);
			hostIPAddress = ipHostEntry.AddressList[0];
		}

		public void Start()
		{
			try
			{
				if (!isRunning)
				{
					isRunning = true;

					tcpListener = new TcpListener(listenerAddress, listenerPort);
					tcpListener.Start();

					var chatRoom = roomPool.Get();
					roomIdToRoom.TryAdd(chatRoom.ID, chatRoom);

					globalRoomID = chatRoom.ID;

					AcceptClientAsync(tcpListener);

					UpdateAsync();

					ServerStarted(listenerAddress, listenerPort, hostName, ipHostEntry);
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		public void Stop()
		{
			try
			{
				if (isRunning)
				{
					isRunning = false;

					if (tcpListener != null)
						tcpListener.Stop();

					var clientTokens = new List<string>(tokenToClient.Keys);
					foreach (var clientToken in clientTokens)
						DisconnectClient(clientToken);

					tokenToClient.Clear();

					ServerStopped();
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task AcceptClientAsync(TcpListener listener)
		{
			try
			{
				while (isRunning)
				{
					var client = await listener.AcceptTcpClientAsync();
					var chatClient = GetChatClient(client);

					ReadClientStreamAsync(chatClient);
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task UpdateAsync()
		{
			try
			{
				while (isRunning)
				{
					await Task.Delay(30);

					await ExecuteQueuedCommands();

					SendQueuedMessages();
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		private async Task ExecuteQueuedCommands()
		{
			ChatCommand command;
			Action<ChatCommand> commandHandler;
			for (int i = 0; commandQueue.Count > 0 && i < commandPoolCapacity; i++)
			{
				if (commandQueue.TryDequeue(out command) &&
					commandTypeToHandler.TryGetValue(command.Type, out commandHandler))
				{
					commandHandler(command);
				}

				commandPool.Put(command);
			}
		}

		#region incoming
		private void HandleRoomNew(ChatCommand command)
		{
			ChatRoom chatRoom = roomPool.Get();
			roomIdToRoom.TryAdd(chatRoom.ID, chatRoom);

			int chatRoomID = chatRoom.ID;

			ChatClient chatClient;
			if (roomIdToRoom.TryGetValue(chatRoomID, out chatRoom) &&
				tokenToClient.TryGetValue(command.Token, out chatClient))
			{
				chatClient.CurrentRoomID = chatRoom.ID;
				chatRoom.Tokens.TryAdd(command.Token, 0);
			}

			RoomCreated(command.Token, chatRoom.ID);
			SendClientRoomList(command.Token);
			SendRoomClientList(command.Token, chatRoom.ID);
		}

		private void HandleRoomClientAdd(ChatCommand command)
		{
			RoomAdditionModel roomAddition = JsonConvert.DeserializeObject<RoomAdditionModel>(command.JSON);

			ChatRoom chatRoom;
			if (roomIdToRoom.TryGetValue(roomAddition.RoomID, out chatRoom) &&
				chatRoom.Tokens.ContainsKey(command.Token)) // is adder part of the room
			{
				if (chatRoom.Tokens.TryAdd(roomAddition.Token, 0))
				{
					RoomClientAdded(roomAddition.Token, roomAddition.RoomID, roomAddition.Token);
					SendClientRoomList(roomAddition.Token);

					foreach (var chatRoomToken in chatRoom.Tokens)
						SendRoomClientList(chatRoomToken.Key, chatRoom.ID);
				}
			}
		}

		private void HandleRoomClientRemove(ChatCommand command)
		{
			RoomRemovalModel roomRemoval = JsonConvert.DeserializeObject<RoomRemovalModel>(command.JSON);

			ChatRoom chatRoom;
			if (roomIdToRoom.TryGetValue(roomRemoval.RoomID, out chatRoom) &&
				chatRoom.Tokens.ContainsKey(command.Token) &&
				command.Token == roomRemoval.Token &&
				roomRemoval.RoomID > 0) // can only remove yourself and if not in global
			{
				byte junk;
				if (chatRoom.Tokens.TryRemove(roomRemoval.Token, out junk))
				{
					RoomClientRemoved(command.Token, roomRemoval.RoomID, roomRemoval.Token);
					SendClientRoomList(command.Token);

					foreach (var chatRoomToken in chatRoom.Tokens)
						SendRoomClientList(chatRoomToken.Key, chatRoom.ID);
				}
			}

			if (chatRoom.Tokens.Count == 0 &&
				roomIdToRoom.TryRemove(chatRoom.ID, out chatRoom))
			{
				roomPool.Put(chatRoom);
			}
		}

		private void HandleRoomClientList(ChatCommand command)
		{
			ClientRoomModel roomModel = JsonConvert.DeserializeObject<ClientRoomModel>(command.JSON);

			ChatClient chatClient;
			if (tokenToClient.TryGetValue(command.Token, out chatClient))
			{
				chatClient.CurrentRoomID = roomModel.RoomID;
			}

			SendRoomClientList(command.Token, roomModel.RoomID);
		}

		private void HandleClientSetName(ChatCommand command)
		{
			ChatClient chatClient;
			if (tokenToClient.TryGetValue(command.Token, out chatClient))
			{
				ClientNameModel data = JsonConvert.DeserializeObject<ClientNameModel>(command.JSON);
				var oldName = chatClient.Name;
				chatClient.Name = data.Name;

				ClientNameChanged(command.Token, oldName, chatClient.Name);

				//TODO for each room the client was in send client list to all members
				data.Token = command.Token;
				var json = JsonConvert.SerializeObject(data);

				ChatRoom chatRoom;
				List<int> clientRoomIDs = GetClientRoomList(command.Token);
				foreach (var roomID in clientRoomIDs)
				{
					if (roomIdToRoom.TryGetValue(roomID, out chatRoom))
					{
						foreach (var chatRoomKVP in chatRoom.Tokens)
							WriteClientStreamAsync(chatRoomKVP.Key, Serialize(ChatCommandType.ClientSetName, json));
					}
				}
			}
		}

		private void HandleClientLogin(ChatCommand command)
		{
			var clientLogin = JsonConvert.DeserializeObject<ClientLoginModel>(command.JSON);

			ChatClient newChatClient = null;
			ChatClient oldChatClient = null;

			if (tokenToClient.TryRemove(clientLogin.Token, out oldChatClient) &&
				tokenToClient.TryRemove(command.Token, out newChatClient))
			{
				// client had a token, remove the old instance of the ChatClient and update the token
				byte junk;
				ChatRoom chatRoom;
				if (roomIdToRoom.TryGetValue(globalRoomID, out chatRoom))
				{
					chatRoom.Tokens.TryRemove(newChatClient.Token, out junk);
					chatRoom.Tokens.TryAdd(oldChatClient.Token, junk);
				}

				newChatClient.Token = oldChatClient.Token;
				newChatClient.Name = oldChatClient.Name;

				tokenToClient.TryAdd(newChatClient.Token, newChatClient);
			}
			else
			{
				// else client didn't have a token carry on
				tokenToClient.TryGetValue(command.Token, out newChatClient);
			}

			if (newChatClient != null)
			{
				LoginClient(newChatClient.Token);

				SendMessageHistory(newChatClient.Token);
				SendClientRoomList(newChatClient.Token);

				//for each room member
				ChatRoom chatRoom;
				if (roomIdToRoom.TryGetValue(newChatClient.CurrentRoomID, out chatRoom))
					foreach (var chatRoomToken in chatRoom.Tokens)
						SendRoomClientList(chatRoomToken.Key, chatRoom.ID);

				if (newChatClient != null)
					ClientConnected(newChatClient.Token);
			}
		}

		private void HandleClientLogout(ChatCommand command)
		{
			DisconnectClient(command.Token);
		}

		private void HandleClientWrite(ChatCommand command)
		{
			var message =
				JsonConvert.DeserializeObject<ClientRoomMessageModel>(command.JSON);

			message.Token = command.Token;
			message.Timestamp = command.Timestamp;
			message.Name = GetClientName(command.Token);

			ChatRoom chatRoom;
			if (roomIdToRoom.TryGetValue(message.RoomID, out chatRoom))
				chatRoom.MessageQueue.Enqueue(message);
		}

		private async Task ReadClientStreamAsync(ChatClient chatClient)
		{
			if (chatClient.Connected)
			{
				try
				{
					TcpClient client = chatClient.TcpClient;
					if (client.Connected)
					{
						CancellationTokenSource readTokenSource = chatClient.ReadCancellationTokenSource;

						NetworkStream stream = client.GetStream();

						byte[] commandBuffer = new byte[4];
						byte[] jsonBytesBuffer;
						byte[] jsonBytesCountBuffer = new byte[4];

						ChatCommandType commandType = ChatCommandType.ClientRoomList;
						int jsonBytesCount = 0;
						int jsonBytesRead = 0;

						while (isRunning && commandType != ChatCommandType.None && chatClient.Connected)
						{
							var command = commandPool.Get();
							if (command != null)
							{
								command.Token = chatClient.Token;
								command.Timestamp = DateTime.UtcNow;

								commandType = await ReadClientStreamCommandAsync(chatClient, commandBuffer, readTokenSource);
								command.Type = commandType;

								if (commandType != ChatCommandType.None)
								{
									jsonBytesCount = await ReadClientStreamMessageLengthAsync(chatClient, jsonBytesCountBuffer, readTokenSource);

									jsonBytesBuffer = new byte[jsonBytesCount];
									if (jsonBytesCount > 0 && stream.CanRead)
										jsonBytesRead = await stream.ReadAsync(jsonBytesBuffer, 0, jsonBytesCount, readTokenSource.Token);

									command.JSON = Encoding.UTF8.GetString(jsonBytesBuffer, 0, jsonBytesCount);

									commandQueue.Enqueue(command);
								}
							}
						}
					}

					DisconnectClient(chatClient.Token);
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.ToString());
					DisconnectClient(chatClient.Token);
				}
			}
		}

		private async Task<ChatCommandType> ReadClientStreamCommandAsync(ChatClient chatClient, byte[] commandBuffer, CancellationTokenSource tokenSource)
		{
			ChatCommandType commandType = ChatCommandType.None;

			if (chatClient != null && chatClient.Connected)
			{
				int charactersRead = 0;

				try
				{
					NetworkStream stream = chatClient.TcpClient.GetStream();
					if (stream.CanRead)
						charactersRead = await stream.ReadAsync(commandBuffer, 0, commandBuffer.Length, tokenSource.Token);
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.ToString());
				}

				if (charactersRead > 0)
				{
					int commandInt = BitConverter.ToInt32(commandBuffer, 0);
					if (Enum.IsDefined(typeof(ChatCommandType), commandInt))
						commandType = (ChatCommandType)commandInt;

					Array.Clear(commandBuffer, 0, commandBuffer.Length);
				}
			}

			return commandType;
		}

		private async Task<int> ReadClientStreamMessageLengthAsync(ChatClient chatClient, byte[] sizeBuffer, CancellationTokenSource tokenSource)
		{
			int messageLength = 0;

			if (chatClient != null && chatClient.Connected)
			{
				int charactersRead = 0;

				try
				{
					NetworkStream stream = chatClient.TcpClient.GetStream();
					if (stream.CanRead)
						charactersRead = await stream.ReadAsync(sizeBuffer, 0, sizeBuffer.Length, tokenSource.Token);
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.ToString());
				}

				if (charactersRead > 0)
					messageLength = BitConverter.ToInt32(sizeBuffer, 0);

				Array.Clear(sizeBuffer, 0, sizeBuffer.Length);
			}

			return messageLength;
		}
		#endregion

		#region outgoing

		public async Task LoginClient(string token)
		{
			var data = new ClientLoginModel()
			{
				Token = token,
					Name = GetClientName(token)
			};
			var json = JsonConvert.SerializeObject(data);

			await WriteClientStreamAsync(token, Serialize(ChatCommandType.ClientLogin, json));
		}

		private async Task SendMessageHistory(string token)
		{
			foreach (var message in messageHistory)
				await SendClientRoomMessage(token, message);
		}

		private void SendQueuedMessages()
		{
			foreach (var item in roomIdToRoom)
			{
				SendQueuedClientRoomMessages(item.Value.MessageQueue, messageQueueCapacity);
			}
		}

		private async Task SendQueuedClientRoomMessages(ConcurrentQueue<ClientRoomMessageModel> messageQueue, int messageQueueCapacity)
		{
			ClientRoomMessageModel message;
			for (int i = 0; messageQueue.Count > 0 && i < messageQueueCapacity; i++)
			{
				if (messageQueue.TryDequeue(out message))
				{
					ChatRoom chatRoom;
					if (roomIdToRoom.TryGetValue(message.RoomID, out chatRoom) &&
						chatRoom.Tokens.ContainsKey(message.Token))
					{
						foreach (var item in chatRoom.Tokens)
							await SendClientRoomMessage(item.Key, message);

						ClientMessageSent(message.Token, message.Name, message.RoomID, message.Message);

						if (message.RoomID == 0)
							ArchiveMessage(message);
					}
				}
			}
		}

		private async Task SendClientRoomMessage(string token, ClientRoomMessageModel message)
		{
			var json = JsonConvert.SerializeObject(message);

			await WriteClientStreamAsync(token, Serialize(ChatCommandType.ClientWrite, json));
		}

		private async Task SendClientRoomList(string token)
		{
			var clientRoomList = new ClientRoomListModel()
			{
				Rooms = new List<ClientRoomModel>()
			};

			ChatRoom chatRoom;
			List<int> clientRoomIDs = GetClientRoomList(token);
			foreach (var roomID in clientRoomIDs)
			{
				if (roomIdToRoom.TryGetValue(roomID, out chatRoom) &&
					chatRoom.Tokens.ContainsKey(token))
				{
					clientRoomList.Rooms.Add(
						new ClientRoomModel() { RoomID = roomID }
					);
				}
			}
			var json = JsonConvert.SerializeObject(clientRoomList);
			await WriteClientStreamAsync(token, Serialize(ChatCommandType.ClientRoomList, json));
		}

		private List<int> GetClientRoomList(string token)
		{
			//TODO should probably cache this
			List<int> clientRoomList = new List<int>();
			foreach (var item in roomIdToRoom)
			{
				if (item.Value.Tokens.ContainsKey(token))
					clientRoomList.Add(item.Key);
			}
			return clientRoomList;
		}

		private async Task SendRoomClientList(string token, int roomID)
		{
			var roomClientList = new RoomClientListModel()
			{
				RoomID = roomID,
					Clients = new List<RoomClientModel>()
			};

			ChatRoom chatRoom;
			if (roomIdToRoom.TryGetValue(roomID, out chatRoom) &&
				chatRoom.Tokens.ContainsKey(token))
			{
				foreach (var item in chatRoom.Tokens)
				{
					ChatClient chatClient = null;

					if (tokenToClient.TryGetValue(item.Key, out chatClient) && chatClient.Connected)
						roomClientList.Clients.Add(
							new RoomClientModel()
							{
								Token = item.Key,
									Name = GetClientName(item.Key)
							}
						);
				}
			}

			var json = JsonConvert.SerializeObject(roomClientList);
			await WriteClientStreamAsync(token, Serialize(ChatCommandType.RoomClientList, json));

			RoomClientListSent(token, roomID);
		}

		private byte[] Serialize(ChatCommandType commandType, string message)
		{
			// response: command [4 bytes] + string[size[4 bytes] + data[size]]
			byte[] commandBytes = Serialize(commandType);
			byte[] messageBytes = Serialize(message);
			return commandBytes.Concat(messageBytes).ToArray();
		}

		private byte[] Serialize(ChatCommandType commandType)
		{
			// command: commandData[4 bytes]
			return BitConverter.GetBytes((int)commandType);
		}

		private byte[] Serialize(string source)
		{
			// string: stringSize[4 bytes] + stringData[stringSize]
			byte[] bytes = Encoding.UTF8.GetBytes(source);
			byte[] bytesSize = BitConverter.GetBytes(bytes.Length);
			return bytesSize.Concat(bytes).ToArray();
		}

		private async Task WriteClientStreamAsync(string token, byte[] responseBytes)
		{
			ChatClient chatClient;
			if (tokenToClient.TryGetValue(token, out chatClient) && chatClient != null && chatClient.Connected)
			{
				try
				{
					NetworkStream stream = chatClient.TcpClient.GetStream();
					if (stream.CanWrite)
						await stream.WriteAsync(responseBytes, 0, responseBytes.Length, chatClient.WriteCancellationTokenSource.Token);
				}
				catch (Exception e)
				{
					Debug.WriteLine(e.ToString());
					DisconnectClient(token);
				}
			}
			else
			{
				DisconnectClient(token);
			}
		}

		#endregion

		#region internal

		private ChatClient GetChatClient(TcpClient tcpClient)
		{
			ChatClient chatClient = new ChatClient();
			tokenToClient.TryAdd(chatClient.Token, chatClient);

			chatClient.TcpClient = tcpClient;
			chatClient.RemoteEndPoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;

			ChatRoom chatRoom;
			if (roomIdToRoom.TryGetValue(globalRoomID, out chatRoom))
				chatRoom.Tokens.TryAdd(chatClient.Token, 0);

			return chatClient;
		}

		private string GetClientName(string token)
		{
			string clientName = string.Empty;
			ChatClient clientDescription;

			if (tokenToClient.TryGetValue(token, out clientDescription))
				clientName = clientDescription.Name;

			return clientName;
		}

		private void ArchiveMessage(ClientRoomMessageModel message)
		{
			ClientRoomMessageModel historicMessage;
			if (messageHistory.Count >= messageHistoryCapacity)
				messageHistory.TryDequeue(out historicMessage);

			messageHistory.Enqueue(message);
		}
		#endregion

		private void DisconnectClient(string token)
		{
			ChatClient chatClient;
			if (tokenToClient.TryGetValue(token, out chatClient))
			{
				//remove client from all rooms and notify the other members
				byte junk;
				foreach (var roomKVP in roomIdToRoom)
				{
					if (roomKVP.Value.Tokens.TryRemove(token, out junk))
					{
						foreach (var chatRoomToken in roomKVP.Value.Tokens)
							SendRoomClientList(chatRoomToken.Key, roomKVP.Value.ID);
					}
				}

				if (chatClient.Disconnect())
				{
					ClientDisconnected(chatClient.Token);
				}
			}
		}
	}
}