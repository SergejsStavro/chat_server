namespace com.bitvillain.chat.server
{
	public class RoomClientModel
	{
		public string Token { get; set; }
		public string Name { get; set; }
	}
}