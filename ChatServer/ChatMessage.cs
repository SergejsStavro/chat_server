using System;
using System.Net;
using System.Text;

namespace com.bitvillain.chat.server
{
	public class ChatMessage
	{
		public string Name { get; set; }
		public string Token { get; set; }
		public int TargetRoomID { get; set; }
		public DateTime Timestamp { get; set; }
		public int Length { get; set; }
		public byte[] Data { get; set; }

		const string StringFormat = "[{0}] {1} on {2} @ {3}: {4}";
		const string DateFormat = "yyyyMMdd";
		const string TimeFormat = "HH:mm:ss.fff";

		public override string ToString()
		{
			return string.Format(
				StringFormat,
				TargetRoomID.ToString(),
				Name,
				// Token.ToString(),
				Timestamp.ToString(DateFormat),
				Timestamp.ToString(TimeFormat),
				Encoding.ASCII.GetString(Data, 0, Length)
			);
		}
	}
}